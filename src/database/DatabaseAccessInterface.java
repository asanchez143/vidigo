package database;

import javax.ejb.Local;

import models.User;

@Local
public interface DatabaseAccessInterface {
	// No error
	public static final int NOERR = 0;
	
	// Generic success and generic failure
	public static final int SUCCESS = 0;
	public static final int FAILURE = 1;
	
	// Search types
	public static final int FIRST = 1;
	public static final int LAST = 2;
	public static final int USERNAME = 3;
	public static final int EMAIL = 4;
	public static final int PASSWORD = 5;
	
	/**
	 * Inserts a new user into the database
	 * @param user The user to insert
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int insertNewUser(User user);
	
	/**
	 * Check if user exists
	 * @param user The user to search for
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int userExists(User user);
	
	/**
	 * Get user by id
	 * @param id The id of the user
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int getUser(String id);
	
	/**
	 * Get user by search value
	 * @param searchType The type of search (FIRST, LAST, USERNAME, etc.)
	 * @param search The value to search for
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int getUserBy(int searchType, String search);
	
	/**
	 * Get user by ANDing two search values
	 * @param searchType The first search type
	 * @param search The first value to search for
	 * @param searchType2 The second search type
	 * @param search2 The second search value
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int getUserBy(int searchType, String search, int searchType2, String search2);
	
	/**
	 * Update a user in the database
	 * @param user User to update. Searches by id
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int updateUser(User user);
	
	/**
	 * Update user by a specific value
	 * @param valueType The value type (FIRST, LAST, USERNAME, etc.)
	 * @param value The value to search for
	 * @param user The user to update with
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int updateUser(int valueType, String value, User user);
	
	/**
	 * Removes a user from the database. Searches by id
	 * @param user The user to remove
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int removeUser(User user);
	
	/**
	 * Removes a user by a specific condition
	 * @param valueType The value type to search for
	 * @param value The value to search for
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int removeUserBy(int valueType, String value);
	
	/**
	 * Authenticate a user. Initially searches by id
	 * @param user The user to authenticate
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int authenticateUser(User user);

	/**
	 * Authenticates user with specified search parameters
	 * @param searchType The type to search for (FIRST, LAST, USERNAME, etc.)
	 * @param value The value to search for
	 * @param password The password (for authentication)
	 * @return Codes. For now, only returns generic success or failure
	 */
	public int authenticateUser(int searchType, String value, String password);
}
