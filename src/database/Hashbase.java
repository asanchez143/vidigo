package database;

import java.util.HashMap;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import models.User;

/**
 * Session Bean implementation class DatabaseAccessInterface
 */
@Stateless
@Local(DatabaseAccessInterface.class)
@Alternative
public class Hashbase implements DatabaseAccessInterface {
	
	private HashMap<String, String> users = new HashMap<String, String>();

	@Override
	public int insertNewUser(User user) {
		users.put(user.getUsername(), user.getPassword());
		return NOERR;
	}

	@Override
	public int authenticateUser(User user) {
		if (users.containsKey(user.getUsername())) {
			return users.get(user.getUsername()).equals(user.getPassword()) ? SUCCESS : FAILURE;
		}
		
		return FAILURE;
	}

	@Override
	public int userExists(User user) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getUser(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getUserBy(int searchType, String search) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getUserBy(int searchType, String search, int searchType2, String search2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateUser(User user) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateUser(int valueType, String value, User user) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int removeUser(User user) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int removeUserBy(int valueType, String value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int authenticateUser(int searchType, String value, String password) {
		// TODO Auto-generated method stub
		return 0;
	}

}
