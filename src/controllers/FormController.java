package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import database.DatabaseAccessInterface;
import models.User;

@ManagedBean
@ViewScoped
public class FormController {
	
		@Inject
		DatabaseAccessInterface database;
		
		/**
		 * Called on user login
		 * @param user The user value passed
		 * @return Next page
		 */
		public String onLogin(User user) {
			
			// Authenticate user then send them to success or failure
			if (database.authenticateUser(user) == DatabaseAccessInterface.SUCCESS) {
				return "success.xhtml";
			}
			
			return "failure.xhtml";
		}
		
		/**
		 * Called on user signup
		 * @param user The user value passed
		 * @return Next page
		 */
		public String onSignup(User user) {
			
			// Authenticate user. On failure, add them to database, then send them to either success or failure
			if (database.authenticateUser(user) == DatabaseAccessInterface.FAILURE) {
				database.insertNewUser(user);
				return "success.xhtml";
			}
			
			return "failure.xhtml";
		}
		
		public DatabaseAccessInterface getService() {
			return database;
		}
}
