package models;

public interface Packageable<T> {
	/**
	 * Gets the value of the package
	 * @return T value
	 */
	public T getValue();
	
	/**
	 * Gets the code of the package
	 * @return int code
	 */
	public int getCode();
	
	/**
	 * Checks to see if flag is set
	 * @param type The type to check for
	 * @return Whether the code exists
	 */
	public boolean getCode(final int type);
	
	/**
	 * Gets all codes from the package
	 * @return int[] codes
	 */
	public int[] getAllCodes();
}
