package models;

public class ErrPackage<T> implements Packageable<T> {
	int error;
	T value;
	
	public ErrPackage(int error, T value) {
		this.error = error;
		this.value = value;
	}
	
	@Override
	public T getValue() {
		return value;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean getCode(int type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int[] getAllCodes() {
		// TODO Auto-generated method stub
		return null;
	}

}
