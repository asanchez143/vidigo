package models;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@ManagedBean
@ViewScoped
public class User {
	@NotNull(message = "Please enter a first name. This is a required field.")
	private String firstName = "";
	
	@NotNull(message = "Please enter a last name. This is a required field.")
	private String lastName = "";
	
	@NotNull(message = "Please enter a username. This is a required field.")
	@Size(min=5, message = "Username must be at least 5 characters long.")
	private String username = "";
	
	@NotNull(message = "Please enter an email. This is a required field.")
	@Email(message = "Email address was not valid.")
	private String email = "";
	
	@NotNull(message = "Please enter a password. This is a required field.")
	@Size(min=6, message = "Password must be at least 6 characters long.")
	private String password = "";
	
	public User() {}
	
	public User(String firstName, String lastName, String username, String email, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.email = email;
		this.password = password;
	}
	
	
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
